# Science Retreat Handbook

Welcome to the repository of the Science Retreat Handbook! :wave:

## How to contribute

You want to contribute? Yay, we'd love that! :clap:


We are using a [project forking
workflow](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)
to include contributions. 
That means, if you want to change the content of the book:

- Fork this repository. 
- This book is written in [Quarto](https://quarto.org). Edit `.qmd` files to add or change content or check out the [Quarto Books Documentation](https://quarto.org/docs/books/) if you need help with adding new chapters or editing other parts of the book.
- Optional: test if changes work. For example by running `quarto preview`in your console or rendering the book in VS Code or RStudio (more details [here](https://quarto.org/docs/books/#book-preview)). 
- Please use meaningful commit messages so that we can track the changes.
- Create a [merge
request](https://gitlab.com/open-science-retreat/science-retreat-handbook/-/merge_requests)
once you are done to make your changes part of the book.
- Celebrate your contribution :tada:

